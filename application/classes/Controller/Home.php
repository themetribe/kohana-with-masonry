<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Home extends Controller {

	public $template;

	public function before(){
		parent::before();
		$this->template = View::factory('home');
			// $this->template->title = 'Kohana Project';
	}

	public function action_index()
	{
		// $members = new Model_Gallery;
                 
      	$query = DB::select()->from('gallery');
      	$results = $query->execute()->as_array();
      	$row = array();
      	foreach ($results as $key => $value) {
      		$row[] = $value;
      	}
      	
      	// print_r($query);
		$this->template->title = 'Kohana Project';
		$this->template->data = $row;
		$this->response->body($this->template);

	}

	public function action_save(){
		if ($this->request->method() == Request::POST)
        {
        	// $this->_save_image($_FILES['image']);
        	$target_dir = DOCROOT.'uploads/';
			$target_file = $target_dir . basename($_FILES["image"]["name"]);
			$uploadOk = 1;
			$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
			
			// Check file size
			if ($_FILES["image"]["size"] > 500000) {
			    echo "Sorry, your file is too large.";
			    $uploadOk = 0;
			}
			// Allow certain file formats
			if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
			&& $imageFileType != "gif" ) {
			    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
			    $uploadOk = 0;
			}
			// Check if $uploadOk is set to 0 by an error
			if ($uploadOk == 0) {
			    echo "Sorry, your file was not uploaded.";
			// if everything is ok, try to upload file
			} else {
			    if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_file)) {
			        
			    	$src = $target_dir.$_FILES["image"]["name"]; 			    	
			    	$dest = $target_dir .basename(("thumbnails-".$_FILES["image"]["name"]));
			    	$desired_width = "200";

			    	 /* read the source image */
				    $source_image = imagecreatefromstring(file_get_contents($src));
				    $width = imagesx($source_image);
				    $height = imagesy($source_image);

				    /* find the "desired height" of this thumbnail, relative to the desired width  */
				    $desired_height = floor($height * ($desired_width / $width));

				    /* create a new, "virtual" image */
				    $virtual_image = imagecreatetruecolor($desired_width, $desired_height);

				    /* copy source image at a resized size */
				    imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);

				    /* create the physical thumbnail image to its destination */
				    imagejpeg($virtual_image, $dest);

				    $title = $this->request->post('image-title');
				    $thumbnail = "thumbnails-".$_FILES["image"]["name"];
				    $filename = $_FILES["image"]["name"];

					$query = DB::insert('gallery', array('title', 'thumbnail', 'filename'))->values(array($title, $thumbnail, $filename));
				    if($query->execute()){
				    	echo "Successfully Save";
				    }else{
				    	echo "Error in insert";
				    }

			    } else {
			        echo "Sorry, there was an error uploading your file.";
			    }
			}

        }else{
           $this->response->body("NO");
        }
	}

} 
