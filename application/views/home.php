<!DOCTYPE html>
<html>
<head>
	<title><?php echo isset($title) ? $title : '' ?></title>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>
<body>
	<nav>
		<a href="">Kohana</a>
	</nav>
	<div >

		<div class="modal">
			<!-- <div class="content-image">
				<img src="" id="image-preview">
				<div class="image-details" style="visibility: hidden;">
						<form id="test" enctype="multipart/form-data">
							<input type="text" class="image-title" name="image-title" value="Hello" placeholder="Image Title">
							<input type="file" name="image" id="file-input" accept="image/*" hidden>

							<br>
							<button class="btn-save">Save</button>	
						</form>
				</div>
				<button class="btn-select-image">Choose Photo</button>
			</div> -->
			<div class="container-add-photo">
				<div class="divider">
					<div class="image-preview">
						<img src="">
					</div>
					<button class="button-select-image">Select Image</button>
				</div>
				<div class="divider">
					<div class=""></div>
					<input type="text" name="">
				</div>
			</div>
		</div>

		<section class="images-section">
			<div id="container">
				<?php foreach($data as $value) :?>
					<div class="grid-item">
						<img  src="<?php echo URL::base(true) . 'uploads/' . $value['filename'] ?>">
					</div>	
				<?php endforeach; ?>
			</div>
		</section>
		<section>
			<ul>
				<?php 
					
					// foreach($data as $key => $value){
					// 	echo $value['title'];
					// }
					
				?>
			</ul>
		</section>
	</div>
	<footer></footer>
</body>
<style type="text/css">
@import url('https://fonts.googleapis.com/css?family=Montserrat');
	body{
		padding: 0;
		margin: 0;
	}
	a{
		color: white;
		text-decoration: none;
		font-family: Montserrat;
		padding: 10px;
		margin: 10px;
	}
	nav{
		width: 100%;
		height: auto;
		line-height: 50px;
		background-color: #00cec9;
		font-size: 15pt;
	}
	
	.images-section{
		margin: 10px
	}
	.content-image{
	    height: 400px;
	    width: 400px;
	    border: 1px solid #c5c5c5;
	    border-radius: 5px;
	    position: relative;
	    margin: auto;
	}
	.btn-select-image{
		padding: 15px;
	    border: 1px solid #00b894;
	    background-color: #00b894;
	    color: #55efc4;
	    font-size: 12pt;
	    letter-spacing: 1.5px;
	    border-radius: 5px;
	    outline: none;
	    cursor: pointer;
	    position: absolute;
	    top: 45%;
	    left: 32.5%;
	}
	.btn-save{
		padding: 10px;
	    border-radius: 3px;
	    border: none;
	    width: 80px;
	    background-color: #00b894;
	    color: #55efc4;
	    margin-top: 10px;
	    font-weight: bolder;
	}
	#image-preview{
	    width: 100%;
	    height: 100%;
		border: none;
	}


	.image-title{
		padding: 15px;
		width: 80%;
		border-radius: 5px;
		border: none;
		background-color: #fff;
		color: black;
		outline: none;
		box-shadow: 1px 1px 1px #503434;
		font-family: Montserrat;
		font-size: 10pt
	}
	#container{
		width: 1000px;
		margin: 0 auto;
	}
	.grid-item > img {
	    height: auto;
	    max-width: 250px
	}
	.grid-item {
		margin: 0 auto;
		padding: 0 10px 10px 0;
		max-width: 250px;
	}
	.modal{
		position: absolute;
	    width: 100%;
	    height: 100%;
	    top: 0;
	    margin: auto;
	    z-index: 999999;
	    background-color: #0000009e;
	}
	.container-add-photo{
		position: relative;
	    top: 20%;
	    width: 50%;
	    height: 60%;
	    background-color: white;
	    margin: 0 auto;
	    display: flex;
	    padding: 20px;
	}
	.divider{
		flex: 1;
		text-align: center;
		padding: 5px;
	}
	.divider:first-child{
		border-right: 2px solid #00cec9;
	}
	.image-preview{
	    width: 300px;
	    height: 300px;
	    margin: auto;
	    border: 1px solid #00000052;
	}

	.image-preview img{
		width: 100%;
		height: : 100%;
	}

	.button-select-image{
		width: 150px;
	    line-height: 40px;
	    margin-top: 15px;
	    border-radius: 6px;
	    border: 1px solid #55efc4;
	    background-color: #00b894;
	    color: white;
	    letter-spacing: 1.2px;
	    outline: none;
	}
	.button-select-image:active{
		border-style: inset;
		border-radius: 6px;
	}


</style>
<script type="text/javascript">
	var load;
	var image;
	$(document).ready(function(){
		$(".btn-select-image").click(function(){
			$("#file-input").trigger("click");
		});
		$("#file-input").change(function(e){
			 e.preventDefault();
			$(".image-details").css("visibility", "visible");
			// Hide the button once the iamge loaded in the screen
			$(".btn-select-image").attr("hidden", true);	

			// Load the image from the input file
			image = $("#file-input").prop('files')[0];
			// Create an object for the image using the information get from the input file
			load = window.URL.createObjectURL;
			// set the src of the image using the file retrieve
			$("#image-preview").attr("src", load(image));

		});

		$(".btn-save").click(function(e){
			e.preventDefault();

			var form = document.querySelector('form');
			var formData = new FormData(form);

	        $.ajax({
	            url: '<?php echo URL::base(true).substr(URL::site("/home/save"), 1)?>',
	            type: 'post',
	            data: formData,
	            contentType: false,
	            processData: false,
	            cache: false, 
	            success: function(response){
	                var container = $("#container");
	                var content = $("#container").html();
	                var append = '<div class="grid-item">'
								 +'<img  src="'+load(image)+'">'
								 +'</div>';
					content = content + append;
					$("#container").empty();
	                $("#container").append(content).masonry("reloadItems").masonry("layout");  
	            },
	        });
		});
	});
</script>
<script type="text/javascript" src="https://unpkg.com/masonry-layout@4.2.2/dist/masonry.pkgd.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.isotope/3.0.6/isotope.pkgd.min.js"></script>
<script src="https://unpkg.com/imagesloaded@4/imagesloaded.pkgd.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	masonry();
});	

function masonry(){
	var $container = $('#container');

	$container.imagesLoaded( function(){
	  $container.masonry({
	      itemSelector: '.grid-item',
  		  // percentPosition: true,
  		  isFitWidth: true,
  		  isAnimated: false,
  		  transitionDuration: 0
	  });
	});
}

</script>

</html