-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 11, 2018 at 04:38 AM
-- Server version: 10.1.35-MariaDB
-- PHP Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kohana`
--

-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

CREATE TABLE `gallery` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `thumbnail` varchar(100) NOT NULL,
  `filename` varchar(100) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gallery`
--

INSERT INTO `gallery` (`id`, `title`, `thumbnail`, `filename`, `date`) VALUES
(6, 'Zumba', 'thumbnails-1_Hhw7rJHvs7YpL4DUbaWGLg.jpeg', '1_Hhw7rJHvs7YpL4DUbaWGLg.jpeg', '2018-10-10 15:36:39'),
(7, 'Hello', 'thumbnails-1_bzaRRu26YN7yDuoQIxc5AQ.jpeg', '1_bzaRRu26YN7yDuoQIxc5AQ.jpeg', '2018-10-10 21:45:43'),
(8, 'Hello', 'thumbnails-0_KUDJOOZl6q8NF1q9.jpg', '0_KUDJOOZl6q8NF1q9.jpg', '2018-10-10 21:47:42'),
(9, 'Hello', 'thumbnails-1_MKcuNNp0WZ-VsnsxdIeltA.jpeg', '1_MKcuNNp0WZ-VsnsxdIeltA.jpeg', '2018-10-10 21:48:06'),
(10, 'Hello', 'thumbnails-1_cUINwPGk2xwuWw06RYxpZA.jpeg', '1_cUINwPGk2xwuWw06RYxpZA.jpeg', '2018-10-10 21:50:27'),
(11, 'Hello', 'thumbnails-1_XQGgLj8mZMzAaI0JySIlMQ.jpeg', '1_XQGgLj8mZMzAaI0JySIlMQ.jpeg', '2018-10-10 21:53:39'),
(12, 'Hello', 'thumbnails-b3089-0njgsvhco2qqqqp92.jpg', 'b3089-0njgsvhco2qqqqp92.jpg', '2018-10-10 22:05:15'),
(13, 'Hello', 'thumbnails-41500285_230184024343977_6310542310677413888_n.jpg', '41500285_230184024343977_6310542310677413888_n.jpg', '2018-10-10 22:06:30'),
(14, 'Hello', 'thumbnails-41468002_230179784344401_1068634884546756608_n.jpg', '41468002_230179784344401_1068634884546756608_n.jpg', '2018-10-10 22:09:58'),
(15, 'Hello', 'thumbnails-41491121_230183154344064_8875894352086302720_n.jpg', '41491121_230183154344064_8875894352086302720_n.jpg', '2018-10-10 22:10:28'),
(16, 'Hello', 'thumbnails-06d5a-1feixtcjnqtqmmtcke4x4iw.jpeg', '06d5a-1feixtcjnqtqmmtcke4x4iw.jpeg', '2018-10-10 22:11:36'),
(17, 'Hello', 'thumbnails-1_g2FlBdZ4nxw-MwZiEzm8NA.jpeg', '1_g2FlBdZ4nxw-MwZiEzm8NA.jpeg', '2018-10-10 22:16:52'),
(18, 'Hello', 'thumbnails-41608619_230178861011160_1921963080019869696_n.jpg', '41608619_230178861011160_1921963080019869696_n.jpg', '2018-10-10 22:23:43'),
(19, 'Hello', 'thumbnails-f98ae-0imr5q84hmoobtpbt.jpg', 'f98ae-0imr5q84hmoobtpbt.jpg', '2018-10-10 22:25:06'),
(20, 'Hello', 'thumbnails-1_9_sAfdXyh1c67cvVP4XuOQ.jpeg', '1_9_sAfdXyh1c67cvVP4XuOQ.jpeg', '2018-10-10 22:29:47'),
(21, 'Hello', 'thumbnails-1_DWUS6ydjhGCOg0K3LklcrQ.jpeg', '1_DWUS6ydjhGCOg0K3LklcrQ.jpeg', '2018-10-10 22:31:43'),
(22, 'Hello', 'thumbnails-41474487_230182221010824_2226911212644335616_n.jpg', '41474487_230182221010824_2226911212644335616_n.jpg', '2018-10-10 22:34:15'),
(23, 'Hello', 'thumbnails-1_wOKFt8kTDvmf72hDiQnUYQ.jpeg', '1_wOKFt8kTDvmf72hDiQnUYQ.jpeg', '2018-10-10 22:37:35'),
(24, 'Hello', 'thumbnails-appealing-men-fashion-dress-shirts-27-casual-street-style-for-8-jpg-v-1491243914.jpg', 'appealing-men-fashion-dress-shirts-27-casual-street-style-for-8-jpg-v-1491243914.jpg', '2018-10-10 22:40:41'),
(25, 'Hello', 'thumbnails-455694d273a933476e054cb827f99e7a.jpg', '455694d273a933476e054cb827f99e7a.jpg', '2018-10-10 22:40:57'),
(26, 'Hello', 'thumbnails-gourmet_restaurant02.jpg', 'gourmet_restaurant02.jpg', '2018-10-10 23:00:54'),
(27, 'Hello', 'thumbnails-download.jpg', 'download.jpg', '2018-10-10 23:04:31'),
(28, 'Hello', 'thumbnails-pochnuva-sezonata-na-blejzeri-eve-go-nashiot-izbor-182416.jpg', 'pochnuva-sezonata-na-blejzeri-eve-go-nashiot-izbor-182416.jpg', '2018-10-10 23:05:10'),
(29, 'Hello', 'thumbnails-supplysummer.jpg', 'supplysummer.jpg', '2018-10-10 23:05:25'),
(30, 'Hello', 'thumbnails-supplysummer.jpg', 'supplysummer.jpg', '2018-10-10 23:06:12'),
(31, 'Hello', 'thumbnails-supplysummer.jpg', 'supplysummer.jpg', '2018-10-10 23:06:13'),
(32, 'Hello', 'thumbnails-supplysummer.jpg', 'supplysummer.jpg', '2018-10-10 23:06:16'),
(33, 'Hello', 'thumbnails-supplysummer.jpg', 'supplysummer.jpg', '2018-10-10 23:06:17'),
(34, 'Hello', 'thumbnails-denim-shirt-and-jeans-men-look.jpg', 'denim-shirt-and-jeans-men-look.jpg', '2018-10-10 23:09:22'),
(35, 'Hello', 'thumbnails-clothes_Pants_180709.jpg', 'clothes_Pants_180709.jpg', '2018-10-10 23:12:17'),
(36, 'Hello', 'thumbnails-455694d273a933476e054cb827f99e7a.jpg', '455694d273a933476e054cb827f99e7a.jpg', '2018-10-10 23:12:56'),
(37, 'Hello', 'thumbnails-455694d273a933476e054cb827f99e7a.jpg', '455694d273a933476e054cb827f99e7a.jpg', '2018-10-10 23:16:22'),
(38, 'Hello', 'thumbnails-rBVaJFnQXw6AA0nhAALxCw0p8n0728.jpg', 'rBVaJFnQXw6AA0nhAALxCw0p8n0728.jpg', '2018-10-10 23:16:38'),
(39, 'Hello', 'thumbnails-rBVaJFnQXw6AA0nhAALxCw0p8n0728.jpg', 'rBVaJFnQXw6AA0nhAALxCw0p8n0728.jpg', '2018-10-10 23:16:43'),
(40, 'Hello', 'thumbnails-rBVaJFnQXw6AA0nhAALxCw0p8n0728.jpg', 'rBVaJFnQXw6AA0nhAALxCw0p8n0728.jpg', '2018-10-10 23:16:45'),
(41, 'Hello', 'thumbnails-gourmet_restaurant02.jpg', 'gourmet_restaurant02.jpg', '2018-10-10 23:18:28'),
(42, 'Hello', 'thumbnails-women-fashion-dresses-women-s-midi-dress.jpg', 'women-fashion-dresses-women-s-midi-dress.jpg', '2018-10-10 23:18:45'),
(43, 'Hello', 'thumbnails-3357c05c5259b8f.jpg', '3357c05c5259b8f.jpg', '2018-10-11 01:05:50'),
(44, 'Hello', 'thumbnails-0_z7D4FkX7SdZRyV9-.png', '0_z7D4FkX7SdZRyV9-.png', '2018-10-11 01:14:56'),
(45, 'Hello', 'thumbnails-033f4-1_7njhfv6ackc1zyv6tkbag.gif', '033f4-1_7njhfv6ackc1zyv6tkbag.gif', '2018-10-11 01:15:56'),
(46, 'TEsting', 'thumbnails-1_W3j__6625hC2R4ozY13oGA.png', '1_W3j__6625hC2R4ozY13oGA.png', '2018-10-11 02:37:22');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `gallery`
--
ALTER TABLE `gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
